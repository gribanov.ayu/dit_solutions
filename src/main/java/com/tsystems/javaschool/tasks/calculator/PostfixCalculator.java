package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Stack;

public class PostfixCalculator {
    private static class DecimalFormatter {
        private final DecimalFormat decimalFormatter;

        public DecimalFormatter(String pattern, Character separator) {
            this.decimalFormatter = new DecimalFormat(pattern);
            this.decimalFormatter.setRoundingMode(RoundingMode.HALF_EVEN);
            DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
            decimalFormatSymbols.setDecimalSeparator(separator);
            this.decimalFormatter.setDecimalFormatSymbols(decimalFormatSymbols);
        }

        public String format(Double digit) {
            return this.decimalFormatter.format(digit);
        }
    }

    private final Double result;

    private static final String PATTERN = "#.####";
    private static final Character SEPARATOR = '.';
    public static final String EQUATION_SPLIT_REGEX = "\\s";
    public static final String MULTIPLY = "*";
    public static final String DIVIDE = "/";
    public static final String MINUS = "-";
    public static final String PLUS = "+";

    public PostfixCalculator(String postfixEquation) {
        this.result = this.solve(postfixEquation);
    }

    public String getResult() {
        if(this.result == null || this.result.isInfinite())
            return null;

        return new DecimalFormatter(PATTERN, SEPARATOR).format(this.result);
    }

    private Double solve(String expression){
        if (expression != null && !expression.isEmpty()) {
            Stack<Double> resultStack = new Stack<>();


            for (String item : expression.split(EQUATION_SPLIT_REGEX)){
                double firstOperand;
                double secondOperand;

                switch (item) {
                    case MULTIPLY:
                        secondOperand = resultStack.pop();
                        firstOperand = resultStack.pop();
                        resultStack.push(firstOperand * secondOperand);
                        break;
                    case DIVIDE:
                        secondOperand = resultStack.pop();
                        firstOperand = resultStack.pop();

                        if(secondOperand == 0.0)
                            return null;

                        resultStack.push(firstOperand / secondOperand);
                        break;
                    case MINUS:
                        secondOperand = resultStack.pop();
                        firstOperand = resultStack.pop();
                        resultStack.push(firstOperand - secondOperand);
                        break;
                    case PLUS:
                        secondOperand = resultStack.pop();
                        firstOperand = resultStack.pop();
                        resultStack.push(firstOperand + secondOperand);
                        break;
                    default:
                        try {
                            resultStack.push(Double.parseDouble(item));
                        } catch (NumberFormatException e) {
                            return null;
                        }
                }
            }
            if (resultStack.size() > 1) {
                return null;
            }
            return resultStack.pop();
        }
        return null;
    }
}
