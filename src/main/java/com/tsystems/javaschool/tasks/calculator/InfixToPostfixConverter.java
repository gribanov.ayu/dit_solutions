package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class InfixToPostfixConverter {
    private static final String INFIX_NOTATION_REGEX = "(?<=[-+*/()])|(?=[-+*/()])";
    public static final String OPENING_BRACKET = "(";
    public static final String CLOSING_BRACKET = ")";

    private final HashMap<String, Integer> operatorPriority  = new HashMap<String, Integer>() {{
        put("/", 2);
        put("*", 2);
        put("+", 1);
        put("-", 1);
        put("(", 0);
    }};

    private final String postfixEquationList;

    public InfixToPostfixConverter(String infixEquation) {
        this.postfixEquationList = this.infixToPostfix(infixEquation);
    }

    public String getPostfixEquationList() {
        return this.postfixEquationList;
    }

    private String infixToPostfix(String statement) {
        if(statement != null) {
            String[] infixEquation = statement.split(INFIX_NOTATION_REGEX);

            if(containsIncorrectSequence(infixEquation)) {
                return null;
            }

            List<String> resultingPostfixEquationList = new LinkedList<>();

            Stack<String> tmpStack = new Stack<>();

            for (String item : infixEquation) {
                if (isNumber(item)) {
                    resultingPostfixEquationList.add(item);
                    continue;
                }

                if (OPENING_BRACKET.equals(item)) {
                    tmpStack.push(item);
                    continue;
                }
                if (CLOSING_BRACKET.equals(item)) {
                    while (!OPENING_BRACKET.equals(tmpStack.peek())) {
                        resultingPostfixEquationList.add(tmpStack.pop());
                    }
                    tmpStack.pop();
                    continue;
                }

                if (this.operatorPriority.containsKey(item)) {
                    while (!tmpStack.empty() &&
                            this.operatorPriority.get(item) <= this.operatorPriority.get(tmpStack.peek()))
                    {
                        resultingPostfixEquationList.add(tmpStack.pop());
                    }
                    tmpStack.push(item);
                    continue;
                }

                return null;
            }

            while (!tmpStack.isEmpty()) {
                resultingPostfixEquationList.add(tmpStack.pop());
            }
            return String.join(" ", resultingPostfixEquationList);
        }
        return null;
    }

    private boolean isNumber(String str) {
        try{
            Double.valueOf(str);
            return true;
        } catch(Exception e){
            return false;
        }
    }

    private boolean containsIncorrectSequence(String[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if(array[i].equals(array[i + 1])) {
                return true;
            }
        }
        return false;
    }
}
