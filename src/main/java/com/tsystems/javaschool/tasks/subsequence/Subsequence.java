package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        try {
            List<Integer> tmp = new ArrayList<Integer>(y);
            tmp.retainAll(x);
            tmp = tmp.stream().distinct().collect(Collectors.toList());

            return tmp.equals(x);
        } catch (Exception e) {
            throw new IllegalArgumentException("EXCEPTION::invalid input (null or empty)");
        }
    }
}
