package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if(inputNumbers.contains(null))
            throw new CannotBuildPyramidException("EXCEPTION::pyramid cannot be build with given input (contains null)!");

        try {
            Collections.sort(inputNumbers);
        } catch (OutOfMemoryError err) {
            throw new CannotBuildPyramidException("EXCEPTION::memory has run out of space!");
        }

        Pyramid pyramid = new Pyramid(inputNumbers.size());
        pyramid.populatePyramidWith(inputNumbers);
        return pyramid.getPyramid();
    }
}
