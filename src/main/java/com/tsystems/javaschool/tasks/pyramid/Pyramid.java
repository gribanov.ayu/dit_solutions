package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class Pyramid {
    private final int[][] pyramid;

    private int pyramidHeight;
    private int pyramidWidth;

    public Pyramid(int inputArraySize) {
        this.pyramidHeight = 0;
        this.pyramidWidth = 0;

        while (true) {
            int pyramidMatrixLength = findLengthFrom(this.pyramidHeight);

            if(pyramidMatrixLength == inputArraySize) {
                this.pyramidWidth = findWidthWith(this.pyramidHeight);
                break;
            }
            if(pyramidMatrixLength > inputArraySize) {
                this.pyramid = null;
                throw new CannotBuildPyramidException("EXCEPTION::pyramid cannot be build with given input (not enough elements)!");
            }

            this.pyramidHeight++;
        }

        this.pyramid = new int[this.pyramidHeight][this.pyramidWidth];
    }

    public int[][] getPyramid() {
        return this.pyramid;
    }

    private int findLengthFrom(int height) {
        int result = 0;
        int i = 1;
        while (i <= height) {
            result += i;
            i++;
        }

        return result;
    }

    private int findWidthWith(int height) {
        int result = -1;
        int i = 0;
        while (i < height) {
            result += 2;
            i++;
        }
        return result;
    }

    public void populatePyramidWith(List<Integer> originalList) {
        if(this.pyramid == null) {
            throw new CannotBuildPyramidException("EXCEPTION::empty pyramid!");
        }

        int begin = 0;
        int middle = this.pyramidWidth / 2;
        int end;

        for (int i = 1; i <= this.pyramidHeight; i++) {
            end = begin + i;

            List<Integer> sliceOfOriginalList = originalList.subList(begin, end);

            putNumbersInRowI(this.pyramid[i - 1], middle, sliceOfOriginalList);

            begin = end;
            middle--;
        }
    }

    private void putNumbersInRowI(int[] row, int middle, List<Integer> elems) {
        for(int i = middle, j = 0; i < row.length && j < elems.size(); j++, i+=2) {
            row[i] = elems.get(j);
        }
    }
}
