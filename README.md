# README #

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : Andrey Gribanov
* Codeship : [![Codeship Status for gribanov.ayu/dit_solutions](https://app.codeship.com/projects/7b168657-5f7d-4d56-b8df-6d053882b9fa/status?branch=master)](https://app.codeship.com/projects/419886)


### Useful links ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* [Codeship](https://codeship.com)